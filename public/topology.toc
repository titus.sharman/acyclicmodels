\contentsline {chapter}{\tocchapter {Chapter}{}{Preface}}{4}{chapter*.1}%
\contentsline {chapter}{\tocchapter {Chapter}{1}{Topological Spaces}}{5}{chapter.1}%
\contentsline {section}{\numberline {1}What is a topological space?}{5}{section.1.1}%
\contentsline {section}{\numberline {2}Examples of Topological Spaces}{9}{section.1.2}%
\contentsline {section}{\numberline {3}Separation and Connectivity}{12}{section.1.3}%
\contentsline {section}{\numberline {4}The Quotient Topology}{13}{section.1.4}%
\contentsline {chapter}{\tocchapter {Chapter}{2}{Homology}}{14}{chapter.2}%
\contentsline {section}{\numberline {1}Simplicial Homology}{14}{section.2.1}%
\contentsline {section}{\numberline {2}Singular Homology}{14}{section.2.2}%
\contentsline {section}{\numberline {3}CW Homology}{14}{section.2.3}%
\contentsline {chapter}{\tocchapter {Chapter}{3}{Forms and De Rahm Cohomology (todo)}}{15}{chapter.3}%
\contentsline {chapter}{\tocchapter {Chapter}{4}{Homotopy (todo)}}{16}{chapter.4}%
\contentsline {section}{\numberline {1}The Fundamental Group: The First Homotopy Group}{16}{section.4.1}%
\contentsline {chapter}{\tocchapter {Chapter}{}{Appendix on Category Theory}}{17}{chapter*.2}%
\contentsline {chapter}{\tocchapter {Chapter}{}{Appendix on Multilinear Algebra}}{18}{chapter*.3}%
\contentsline {chapter}{\tocchapter {Chapter}{}{Bibliography}}{19}{chapter*.4}%
